/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.gousiosg.javacg.stat;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author expeditive
 */
public class Node {

    private String name;

    private transient String type;

    public Node() {

    }

    public Node(String name) {

        this.name = name;
    }
    
    public Node(String name,String type) {

        this.name = name;
        
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Node> getChildren() {
        return children;
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    private List<Node> children;

    public void addChildren(Node node) {

        if (children == null) {
            children = new ArrayList<>();
        }

        children.add(node);

    }

    boolean hasChildren() {
        return children != null && children.size() > 0;
    }

   public Node getChildByName(String pckgName) {

        if (this.children != null) {

            Optional<Node> opt = this.children.stream().filter((t) -> {
                return t.name.equalsIgnoreCase(pckgName);
            }).findAny();

            if (opt.isPresent()) {

                return opt.get();
            }
        }
        return null;

    }
   
   public Node getParentByChildName(String pckgNameChild) {

        if (this.children != null) {

            Optional<Node> opt = this.children.stream().filter((parentPckg) -> {
                
                return pckgNameChild.toLowerCase().startsWith(parentPckg.name.toLowerCase());
                
            }).findAny();

            if (opt.isPresent()) {

                return opt.get();
            }
        }
        return null;

    }

}
