/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.gousiosg.javacg.stat;

/**
 *
 * @author expeditive
 */
class JCallGraphBase {

    protected static String packageNameIncludePattern;

    protected static Node rootNode;

    protected static void showNodes(Node node) {

        if (node.getType().equals("method")) {

            System.out.print(" ->");

        }

        System.out.println(node.getType() + "  " + node.getName());

        if (node.hasChildren()) {

            node.getChildren().forEach((nodeChild) -> {

                showNodes(nodeChild);

            });

        }

    }

    public String getJsonResult() {

        String json = JsonParser.getEntityAsJSONString(rootNode);

        return json;

    }

}
