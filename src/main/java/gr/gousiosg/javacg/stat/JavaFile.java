/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.gousiosg.javacg.stat;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.veronika.dcm.ApplicationConfig;
import static gr.gousiosg.javacg.stat.JCallGraphBase.rootNode;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

/**
 *
 * @author expeditive
 */
public class JavaFile {

    public void parse(Path javaFile) {

        try {

            CompilationUnit compilationUnit = JavaParser.parse(javaFile);

            String pckgName = "default";

            Optional<PackageDeclaration> optPackage = compilationUnit.getPackageDeclaration();

            if (optPackage.isPresent()) {

                pckgName = optPackage.get().getNameAsString();

            }

            Node pkgNode = rootNode.getChildByName(pckgName);

            if (pkgNode == null) {

                pkgNode = new Node(pckgName, "package");
                
                rootNode.addChildren(pkgNode);

            }

            NodeList<TypeDeclaration<?>> types = compilationUnit.getTypes();

            for (TypeDeclaration type : types) {

                processType(pkgNode, type);

            }

        } catch (IOException ex) {

            ApplicationConfig.getLogger().error(ex);

        }

    }

    public void processType(Node pkgNode, TypeDeclaration<?> type) {

        Node classNode = new Node(type.getNameAsString(), "class");

        pkgNode.addChildren(classNode);

        type.getMethods().forEach((method) -> {
            processMethod(classNode, method);
        });
    }

    private void processMethod(Node classNode, MethodDeclaration method) {

        Node methodNode = new Node(method.getNameAsString(), "method");

        classNode.addChildren(methodNode);

        MethodCallVisitor visitor = new MethodCallVisitor();
        visitor.setMethodNode(classNode);

        method.accept(visitor, null);

    }

}

class MethodCallVisitor extends VoidVisitorAdapter<Void> {

    private Node methodNode;

    @Override
    public void visit(MethodCallExpr n, Void arg) {

        methodNode.addChildren(new Node(n.getScope() + ":" + n.getName() + "( )", "method"));

    }

    void setMethodNode(Node methodNode) {
        this.methodNode = methodNode;
    }
}
