/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
/**
 * Package for Utilities classes
 * with general purposes.
 *
 * @author Rafael Benedettelli
 */
package com.veronika.utils;
