/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.veronika.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utility Helper class for manage java date in the project such as Date mask,
 * formats and several transformations.
 *
 *
 * @author Rafael Benedettelli
 */
public final class UtilsDate {

    /**
     * Get the current date in the string format applying the default mask
     * indicated in the property file of the project.
     *
     * @return String represents the current date
     */
    public static String getCurrentFormateDate() {

        Date d = new Date();

        SimpleDateFormat sp = new SimpleDateFormat(
                ApplicationParameters.CONF_REST_DATE_FORMAT_MASK);

        String date = sp.format(d);

        return date;
    }

    /**
     * Formate a date to the string representation applying the default mask
     * indicated in the property file of the project.
     *
     * @param date the date parameter object
     * @return String represents the date passed in the string format
     */
    public static String formatDate(Date date) {

        DateFormat dateFormat = new SimpleDateFormat(
                ApplicationParameters.CONF_REST_DATE_FORMAT_MASK);

        return dateFormat.format(date);

    }

}
