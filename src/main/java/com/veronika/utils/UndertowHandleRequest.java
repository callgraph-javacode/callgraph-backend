/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.veronika.utils;

import io.undertow.UndertowLogger;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.server.handlers.form.FormParserFactory;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Deque;
import java.util.Map;
import org.jboss.logging.BasicLogger;

/**
 *
 * @author expeditive
 */
public final class UndertowHandleRequest {

    private UndertowHandleRequest() {

    }

    /**
     *
     * @param exchange
     * @return
     * @throws IOException
     */
    public static String getBodyMessageFromRequest(
            final HttpServerExchange exchange) throws IOException {

        BasicLogger logger = UndertowLogger.REQUEST_LOGGER;

        exchange.startBlocking();

        long bodySize = exchange.getRequestContentLength();

        ByteBuffer buffer = ByteBuffer.allocate((int) bodySize);

        exchange.getRequestChannel().read(buffer);

        String bodyMessage = new String(
                buffer.array(), StandardCharsets.UTF_8);

        logger.debug(bodyMessage);

        return bodyMessage;

    }

    /**
     *
     * @param exchange
     * @param name
     * @return
     */
    public static String getPathParameter(
            final HttpServerExchange exchange, final String name) {

        Map<String, Deque<String>> params = exchange.getQueryParameters();
        Deque<String> idParam = params.get(name);
        String idValue = idParam.getFirst();

        return idValue;

    }

    /**
     *
     * @param exchange
     * @return
     * @throws Exception
     */
    public static FormDataParser getFormDataParser(
            final HttpServerExchange exchange) throws Exception {

        final FormParserFactory parse = FormParserFactory.builder().build();

        final FormDataParser parser = parse.createParser(exchange);

        return parser;

    }

}
