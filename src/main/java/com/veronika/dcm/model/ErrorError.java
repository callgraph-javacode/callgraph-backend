/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.veronika.dcm.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author expeditive
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.UndertowCodegen", date = "2018-02-23T21:39:08.249Z")
public class ErrorError {

    private String errorKey = null;
    private Integer statusCode = null;
    private String briefSummary = null;
    private String stackTrace = null;
    private String descriptionURL = null;
    private String logId = null;

    /**
     *
     * @param errorKey
     * @return  
     */
    public ErrorError errorKey(String errorKey) {
        this.errorKey = errorKey;
        return this;
    }

    /**
     *
     * @return
     */
    @JsonProperty("errorKey")
    public String getErrorKey() {
        return errorKey;
    }

    /**
     *
     * @param errorKey
     */
    public void setErrorKey(String errorKey) {
        this.errorKey = errorKey;
    }

    /**
     *
     * @param statusCode
     * @return  
     */
    public ErrorError statusCode(Integer statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    /**
     *
     * @return
     */
    @JsonProperty("statusCode")
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     *
     * @param statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     *
     * @param briefSummary
     * @return 
     */
    public ErrorError briefSummary(String briefSummary) {
        this.briefSummary = briefSummary;
        return this;
    }

    /**
     *
     * @return
     */
    @JsonProperty("briefSummary")
    public String getBriefSummary() {
        return briefSummary;
    }

    /**
     *
     * @param briefSummary
     */
    public void setBriefSummary(String briefSummary) {
        this.briefSummary = briefSummary;
    }

    /**
     *
     * @param stackTrace
     * @return  
     */
    public ErrorError stackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
        return this;
    }

    /**
     *
     * @return
     */
    @JsonProperty("stackTrace")
    public String getStackTrace() {
        return stackTrace;
    }

    /**
     *
     * @param stackTrace
     */
    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    /**
     *
     * @param descriptionURL
     * @return  
     */
    public ErrorError descriptionURL(String descriptionURL) {
        this.descriptionURL = descriptionURL;
        return this;
    }

    /**
     *
     * @return
     */
    @JsonProperty("descriptionURL")
    public String getDescriptionURL() {
        return descriptionURL;
    }

    /**
     *
     * @param descriptionURL
     */
    public void setDescriptionURL(String descriptionURL) {
        this.descriptionURL = descriptionURL;
    }

    /**
     *
     * @param logId
     * @return 
     */
    public ErrorError logId(String logId) {
        this.logId = logId;
        return this;
    }

    /**
     *
     * @return
     */
    @JsonProperty("logId")
    public String getLogId() {
        return logId;
    }

    /**
     *
     * @param logId
     */
    public void setLogId(String logId) {
        this.logId = logId;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ErrorError errorError = (ErrorError) o;
        return Objects.equals(errorKey, errorError.errorKey)
                && Objects.equals(statusCode, errorError.statusCode)
                && Objects.equals(briefSummary, errorError.briefSummary)
                && Objects.equals(stackTrace, errorError.stackTrace)
                && Objects.equals(descriptionURL, errorError.descriptionURL)
                && Objects.equals(logId, errorError.logId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(errorKey, statusCode, briefSummary, stackTrace, descriptionURL, logId);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ErrorError {\n");

        sb.append("    errorKey: ").append(toIndentedString(errorKey)).append("\n");
        sb.append("    statusCode: ").append(toIndentedString(statusCode)).append("\n");
        sb.append("    briefSummary: ").append(toIndentedString(briefSummary)).append("\n");
        sb.append("    stackTrace: ").append(toIndentedString(stackTrace)).append("\n");
        sb.append("    descriptionURL: ").append(toIndentedString(descriptionURL)).append("\n");
        sb.append("    logId: ").append(toIndentedString(logId)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
