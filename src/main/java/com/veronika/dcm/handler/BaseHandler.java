/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.veronika.dcm.handler;

import com.veronika.dcm.ApplicationConfig;
import static com.veronika.dcm.ApplicationConfig.I18N;
import com.veronika.dcm.model.ErrorError;
import com.veronika.utils.ApplicationParameters;
import com.veronika.utils.JsonParser;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import java.util.Arrays;
import org.jboss.logging.BasicLogger;

/**
 *
 * @author expeditive
 */
public abstract class BaseHandler {

    /**
     *
     */
    public static String BASE_PATH;

    /**
     *
     */
    protected static String bodyMessage;

    /**
     * Undertow Logger.
     */
    protected static BasicLogger logger = ApplicationConfig.getLogger();

    /**
     * Basic constructor with logic for
     * generate the API base path from the keys 
     * defined in properties files.
     */
    public BaseHandler() {

        final String BAR = "/";

        BASE_PATH = BAR + ApplicationParameters.CONF_DCM_ARTIFACT + BAR
                + ApplicationParameters.CONF_DCM_SCOPE + BAR
                + ApplicationParameters.CONF_DCM_NAME + BAR
                + ApplicationParameters.CONF_DCM_VERSION;

        logger.info("Initalizate server at: " + BASE_PATH);

    }

    /**
     *
     * @param exchange
     * @param statusCode
     * @param e
     * @return
     */
    public static ErrorError sendError(HttpServerExchange exchange, int statusCode, Exception e) {

        ErrorError error = sendError(exchange, statusCode, e.getMessage());

        error.setStackTrace(Arrays.toString(e.getStackTrace()));
        
        logger.error("Error in application: " + e.getMessage());

        return error;

    }

    /**
     *
     * @param exchange
     * @param statusCode
     * @param brief
     * @return
     */
    public static ErrorError sendError(HttpServerExchange exchange, int statusCode, String brief) {

        exchange.setStatusCode(statusCode);
        ErrorError error = new ErrorError();
        error.setBriefSummary(brief);
        error.setErrorKey(brief);
        error.setStatusCode(statusCode);
        error.setDescriptionURL(exchange.getRequestURL());
        error.setStackTrace(I18N.getString("global.no_stack_trace"));

        exchange.getResponseHeaders()
                .put(Headers.CONTENT_TYPE, "application/json");

        exchange.getResponseSender()
                .send(JsonParser.getEntityAsJSONString(error));

        logger.error("Error in application: " + brief);

        return error;
    }

}
