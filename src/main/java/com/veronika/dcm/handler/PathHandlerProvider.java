/*
 * Copyright (c) 2017, 2018, TKI and/or Villmond. All rights reserved.
 * TKI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.veronika.dcm.handler;

import com.networknt.server.HandlerProvider;
import com.veronika.dcm.ApplicationConfig;
import static com.veronika.dcm.handler.BaseHandler.BASE_PATH;
import com.veronika.dcm.model.Server;
import com.veronika.utils.ApplicationParameters;
import static com.veronika.utils.ApplicationParameters.HOME_UI;
import com.veronika.utils.Messages;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.UndertowOptions;
import io.undertow.server.HttpHandler;
import io.undertow.server.RoutingHandler;
import io.undertow.server.handlers.PathHandler;
import java.net.InetAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

/**
 *
 * @author expeditive
 */
@Service
@Configuration
@ComponentScan({"com.veronika.dcm"})
public class PathHandlerProvider implements HandlerProvider {

    @Autowired
    SwaggerHandlerProvider swaggerHandlerProvider;

    @Autowired
    CallGraphHandlerProvider callGraphHandlerProvider;

    private static Undertow undertowServer;

    /**
     * Setter for embed mode.
     */
    public static void setEmbedMode() {

        ApplicationParameters.setConfDcmDatabaseModal("EMBED");

    }

    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception {

        ApplicationConfig.getLogger().debug("Init application");

        if (ApplicationParameters.getConfDcmDatabaseModal() == null) {

            ApplicationParameters.setConfDcmDatabaseModal(Messages.getString(
                    "conf.dcm.database.modal"));

        }

        ApplicationConfig.init();

        Server serverConfig = ApplicationConfig.server;

        PathHandlerProvider pathHandler = ApplicationConfig.getBean(PathHandlerProvider.class);

        HttpHandler handler = pathHandler.getHandler();

        undertowServer = Undertow.builder()
                .addHttpListener(Integer.parseInt(serverConfig.getPort()),
                        "0.0.0.0")
                .setHandler(handler)
                .setServerOption(UndertowOptions.URL_CHARSET, "UTF-8")
                .build();

        undertowServer.start();

        String url = "http://" + InetAddress.getLocalHost().getHostAddress() + ":" + serverConfig.getPort();

        ApplicationConfig.getLogger().info("Init Undertow Server");
        ApplicationConfig.getLogger().info("REST Services in " + url + BASE_PATH);
        ApplicationConfig.getLogger().info("Listening in " + url + HOME_UI);
    }

    /**
     * This method stop the server.
     */
    public static void stopServer() {

        ApplicationConfig.getLogger().info("Stoping Undertow Server");

        undertowServer.stop();

    }

    /**
     *
     * @return
     */
    @Override
    public HttpHandler getHandler() {

        ApplicationConfig.getLogger().debug("Get handler process");

        RoutingHandler routingHandler = Handlers.routing();

        callGraphHandlerProvider.addHandler(routingHandler);

        PathHandler pathHandlers = swaggerHandlerProvider.addHandler().
                addPrefixPath("/", routingHandler);

        return pathHandlers;

    }

}
