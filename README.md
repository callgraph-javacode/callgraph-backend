# Java callgraph generator #

This solutions generates a Java callgraph graphic from JAR, EAR, ZIP or github source repository.


## Specifications ##
Java 8, Maven, Java reflection.

## Wiki ##
<a href="https://gitlab.com/callgraph-javacode/callgraph-backend/wikis/Java-Callgraph">see the wiki</a>


## Author ##

Ing. Rafael Benedettelli

veronika.com.ar

rblbene@gmail.com


